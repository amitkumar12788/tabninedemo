package org.example;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        Map<String, String> map = new java.util.HashMap<String, String>();
        map.put("key1", "value1");
        map.put("key2", "value2");
        map.put("key3", "value3");

        List<User> lstUser = new java.util.ArrayList<User>();
        lstUser.add(new User( 1, "user1","a@a.com","pwd","123456789" ));


        // convert users to map object
        Map<String, User> mapUser = lstUser.stream().collect(java.util.stream.Collectors.toMap(User::getName, u -> u));
        System.out.println(mapUser);
        System.out.println(mapUser.get("user1"));

        // convert users to map using id as key
        Map<Integer, User> mapUser2 = lstUser.stream().collect(java.util.stream.Collectors.toMap(User::getId, u -> u));
        System.out.println(mapUser2);
        System.out.println(mapUser2.get(1));

        // convert users to set
        Set<User> setUser = lstUser.stream().collect(java.util.stream.Collectors.toSet());
        System.out.println(setUser);
        System.out.println(setUser.contains(new User( 1, "user1","a@a.com","pwd","123456789" )));





    }
}
