package org.example;

public class Address {
    private String street;
    private String city;
    private String state;
    private String zip;
    private String country;

    public Address(String street, String city, String state, String zip, String country) {
        this.street = street;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.country = country;

    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    // create a toString method
    @Override
    public String toString() {
        return "Address{" + "street=" + street + ", city=" + city + ", state=" + state + ", zip=" + zip + ", country=" + country + '}';
    }

    // create a equals method
    @Override
    public boolean equals(Object obj) {
           this.street = ((Address) obj).getStreet();
           this.city = ((Address) obj).getCity();
           this.state = ((Address) obj).getState();
           this.zip = ((Address) obj).getZip();
           this.country = ((Address) obj).getCountry();
           return this.street.equals(this.street) && this.city.equals(this.city) && this.state.equals(this.state) && this.zip.equals(this.zip) && this.country.equals(this.country);

    }

    // create a hashCode method
    @Override
    public int hashCode() {
        return this.street.hashCode() + this.city.hashCode() + this.state.hashCode() + this.zip.hashCode() + this.country.hashCode();
    }

}
